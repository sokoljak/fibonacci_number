package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Fibonacci sequence calculator!")
	fmt.Println("Give number to calculate")

	var n int

	_, err := fmt.Scan(&n)

	if err != nil {
		panic(err)
	}

	if n < 0 {
		panic("n cannot be less than zero")
	}

	fmt.Println("Calculation result")
	//executeRecursive(n) you don't want to wait for that xD
	executeOptimal(n)

}

func executeRecursive(n int) {
	start := time.Now()
	v := calculateRecursive(n)
	duration := time.Since(start)
	fmt.Printf("Recursive method - value: %d time: %d ms\n", v, duration.Milliseconds())
}

func executeOptimal(n int) {
	start := time.Now()
	v := calculateOptimal(n)
	duration := time.Since(start)
	fmt.Printf("Optimal method - value: %d time: %d ms\n", v, duration.Milliseconds())
}


func calculateRecursive(n int) int64 {
	if n == 0 {
		return 0
	}

	if n == 1 {
		return 1
	}

	return calculateRecursive(n-1) + calculateRecursive(n-2)
}


func calculateOptimal(n int) (v int64) {
	if n == 0 {
		return 0
	}

	if n == 1 {
		return 1
	}

	var x int64 = 0
	var y int64 = 1

	for i := 2; i <= n; i++ {
		v = x + y
		x = y
		y = v
	}

	return v
}
